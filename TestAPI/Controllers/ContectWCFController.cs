using Microsoft.AspNetCore.Mvc;
using TestServiceReference;

namespace TestAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ContectWCFController : ControllerBase
    {
        private readonly ITestService _testService;

        public ContectWCFController(ITestService testService)
        {
            _testService = testService;
        }


        [HttpGet]
        public async Task<string> AddAsync()
        {
            var id = await _testService.CountAsync() + 1;
            var uuid = Guid.NewGuid().ToString("N");
            var addTime = DateTime.Now;
            var index = await _testService.AddAsync(new TestModel() { ID = id, UUID = uuid ,AddTime = addTime });
            return $"ID: {id}, UUID: {uuid}, AddTime: {addTime} {Environment.NewLine} index:{index}";
        }

        [HttpGet]
        public async Task<TestModel> GetAsync(int index)
        {
            return await _testService.GetAsync(index);
        }
    }
}