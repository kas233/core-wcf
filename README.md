# CoreWCF

### TestAPI

> 测试wcf服务调用

添加服务引用【WCF Web Service】

![image-20230211084958450](https://kas233-image-host.oss-cn-shanghai.aliyuncs.com/img/image-20230211084958450.png)

### TestWCF

> 测试.net core WCF服务搭建

![image-20230211085454348](https://kas233-image-host.oss-cn-shanghai.aliyuncs.com/img/image-20230211085454348.png)

[DigDes/SoapCore: SOAP extension for ASP.NET Core (github.com)](https://github.com/DigDes/SoapCore)

从nuget添加`SoapCore`
