﻿using System.Runtime.Serialization;

namespace TestWCF.Model;

[DataContract]
public class TestModel
{
    [DataMember]
    public int ID { get; set; }
    
    [DataMember]
    public string UUID { get; set; }

    [DataMember]
    public DateTime AddTime { get; set; }
}