﻿using System.ServiceModel;
using TestWCF.Model;

namespace TestWCF.Contract;
[ServiceContract]
public interface ITestService
{
    [OperationContract]
    TestModel Get(int index);
    

    [OperationContract]
    int Add(TestModel model); 

    [OperationContract]
    int Count();
}
