﻿using TestWCF.Contract;
using TestWCF.Model;

namespace TestWCF.Service;

public class TestService : ITestService
{
    private readonly HashSet<TestModel> _testModels = new HashSet<TestModel>();

    public int Add(TestModel model)
    {
        _testModels.Add(model);
        return _testModels.Count - 1;
    }

    public TestModel Get(int index)
    {
        return _testModels.ElementAt(index);
    }

    public int Count()
    {
        return _testModels.Count;
    }
}
